//  Author:Alexander Mishin
//  Date:06 Nov, 2019
//
/*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

//#include <typeinfo>

#include "tea5767.h"

TEA5767::TEA5767(
    enum tea5767_clock clock,
    const char* iicDevice,
    uint8_t slaveAddress
) : iic_handle(open(iicDevice, O_RDWR))
{

  switch(clock)
  {
    case tea5767_clock::_13000_KHz:
      this->xtal = this->pllRef = false;
      this->refFreqHz = 50000;
      this->gridStep = 100000;
      break;
    case tea5767_clock::_6500_KHz:
      this->xtal = false;
      this->pllRef = true;
      this->refFreqHz = 50000;
      this->gridStep = 100000;
      break;
    case tea5767_clock::_32768_Hz:
      this->xtal = true;
      this->pllRef = false;
      this->refFreqHz = 32768;
      this->gridStep = 98304;
      break;
  };

  this->Cntrl.XTAL = this->xtal;
  this->Cntrl.PLLREF = this->pllRef;

  if ( this->iic_handle == -1)
  {
    perror(iicDevice);
    exit(EXIT_FAILURE);
  };

  slave_addr = slaveAddress << 1; // Read only
};

TEA5767::~TEA5767()
{
  close(iic_handle);
};

void
TEA5767::init(tea5767_band band)
const
{
  this->setBand(band);
  this->setSearchStopLevel(SSL::Middle);
  this->setSearchDirection(tea5767_sud::Up);
  this->setSideInjection(tea5767_hlsi::High);
  this->setSWPort1(false);
  this->setSWPort2(false);
  this->Cntrl.MS = false; // Stereo
  this->Cntrl.SMUTE = false;
  this->Cntrl.SNC = true;
  this->Cntrl.HCC = true;
  this->Cntrl.SI = false;
  this->setStandby(false);
  this->Cntrl.DTC =  false;
//  setFrequency(this->band_start);
};

/* Write up to five bytes from a buffer to a device */
bool
TEA5767::iic_write(uint8_t *buffer, uint16_t size)
const
{
  struct iic_msg msg[1] =
  {
    {this->slave_addr, IIC_M_WR, size, buffer}
  };
  struct iic_rdwr_data xfer = {msg, 1};

  if ( ioctl(this->iic_handle, I2CRDWR, &xfer) == -1 )
  {
    perror("ioctl(I2CRDWR)");
    return(false);
  };

  return(true);
};

/* Read up to five bytes from a device to buffer */
bool
TEA5767::iic_read(uint8_t *buffer, uint16_t size)
const
{
  struct iic_msg msgs[1] =
  {
    {this->slave_addr, IIC_M_RD, size, buffer}
  };
  struct iic_rdwr_data xfer = {msgs, 1};

  if ( ioctl(this->iic_handle, I2CRDWR, &xfer) == -1)
  {
    perror("ioctl(I2CRDWR)");
    return(false);
  };

  return(true);
};

/* Receive five registers starting at 0 */
bool
TEA5767::recvRegisters(uint8_t size)
const
{
  if(size < 1 || size > 5) size = 5;
  return(this->iic_read(this->readRegisters, size));
};

/* Send five registers starting at 0 */
bool
TEA5767::sendRegisters(uint8_t size)
const
{
  if(size < 1 || size > 5) size = 5;
  return(this->iic_write(this->writeRegisters, size));
};

/* Print status for debug */
void
TEA5767::printState(void)
const
{
  printf("Read registers: %02x %02x %02x %02x %02x\n",
    this->readRegisters[0],
    this->readRegisters[1],
    this->readRegisters[2],
    this->readRegisters[3],
    this->readRegisters[4]
  );
  printf("Write registers: %02x %02x %02x %02x %02x\n",
    this->writeRegisters[0],
    this->writeRegisters[1],
    this->writeRegisters[2],
    this->writeRegisters[3],
    this->writeRegisters[4]
  );
  printf("Device is %sready\n", this->State.RF ? "" : "not ");
  printf("Band level %sreached\n", this->State.BLR ? "" : "not ");
  printf("Mode: %s\n", this->State.STEREO ? "Stereo" : "Mono");
  printf("Intermediate frequency: %d\n", this->State.IF);
  printf("Chip ID: %d\n", this->State.CI);
  printf("Strength: %d\n", this->getSignalLevel());
  printf("Side injection: %s\n", this->Cntrl.HLSI ? "high" : "low");

  printf("Frequency (MGz): %f\n", this->getFrequency());
};

/* Get a RF signal level */
uint8_t
TEA5767::getSignalLevel(void)
const
{
  return this->State.LEV;
};

/* Calculate a frequency MGz */
double
TEA5767::getFrequency(void)
const
{
  this->recvRegisters();
  long int freqHz = this->get_frequency(static_cast<tea5767_hlsi>(this->Cntrl.HLSI));

  return static_cast<double>(freqHz) / 1000000;
};

/* Get a frequency with taking in account an intermediate frequency */
long int
TEA5767::get_frequency(tea5767_hlsi hlsi)
const
{
  long int freqHz;

  if(hlsi == tea5767_hlsi::High)
    freqHz = this->get_frequency(+TEA5767_HLSI_IF);
  else
    freqHz = this->get_frequency(-TEA5767_HLSI_IF);

  return freqHz;
};

/* Get a frequency with taking in account an HLSI flag */
long int
TEA5767::get_frequency(long int imFreqHz)
const
{
  uint16_t freq = (this->State.hiPLL << 8) | this->State.loPLL;
  return this->refFreqHz / 4 * freq - imFreqHz;
};

/* Set given frequency w/[o] a side injection optimizing */
void
TEA5767::setFrequency(double freqMHz, bool tune)
const
{
  tea5767_hlsi si;
  bool mute_status = this->Cntrl.MUTE; // Save until end of method
  long int freqHz = freqMHz * 1000000;

  if(freqHz < this->band_start) freqHz = this->band_start;
  if(freqHz > this->band_stop) freqHz = this->band_stop;

  this->Cntrl.MUTE = true;
  if(tune) // If tune is not needed
    si = this->side_injection(freqHz);
  else
    si = static_cast<tea5767_hlsi>(this->Cntrl.HLSI);

  this->set_frequency(freqHz, si);

  this->Cntrl.MUTE = mute_status;
  this->sendRegisters();
};

/* Set a frequency with taking in account an intermediate frequency */
void
TEA5767::set_frequency(long int freqHz, long int imFreqHz)
const
{
  uint16_t freq = lroundf(static_cast<double>(4 * (freqHz + imFreqHz)) / this->refFreqHz);

  this->Cntrl.hiPLL = freq >> 8;
  this->Cntrl.loPLL = freq & 0xff;
};

/* Set a frequency with taking in account an HLSI flag */
void
TEA5767::set_frequency(long int freqHz, tea5767_hlsi hlsi)
const
{
  this->setSideInjection(hlsi);
  if(hlsi == tea5767_hlsi::High)
    this->set_frequency(freqHz, +TEA5767_HLSI_IF);
  else
    this->set_frequency(freqHz, -TEA5767_HLSI_IF);
};

/* Load an actual frequency from device and fill to registers for write */
void
TEA5767::load_frequency(void)
const
{
  this->Cntrl.hiPLL = this->State.hiPLL;
  this->Cntrl.loPLL = this->State.loPLL;
};

/* Detect side injection mode */
tea5767_hlsi
TEA5767::side_injection(long int freqHz)
const
{
  uint8_t siHiLevel = 0;
  uint8_t siLoLevel = 0;

  // Try High side injection
  this->setSideInjection(tea5767_hlsi::High);
  this->set_frequency(freqHz, +TEA5767_HLSI_IF * 2);
  this->sendRegisters(3);

  usleep(TEA5767_LEV_TIMEOUT);

  if(this->recvRegisters())
    siHiLevel = this->getSignalLevel();

  // Try Low side injection
  this->setSideInjection(tea5767_hlsi::Low);
  this->set_frequency(freqHz, -TEA5767_HLSI_IF * 2);
  this->sendRegisters(3);

  usleep(TEA5767_LEV_TIMEOUT);

  if(this->recvRegisters())
    siLoLevel = this->getSignalLevel();

  return static_cast<tea5767_hlsi>(siHiLevel < siLoLevel);
};

/* Search a next station in given direction */
bool
TEA5767::nextStation(tea5767_sud direction)
const
{
  bool end_of_band = false;
  bool stop_search = false;
  long int freqHz = this->get_frequency();
  bool mute_status = this->Cntrl.MUTE; // Save until end of method

  // Mute also must be set by datasheet
  this->Cntrl.MUTE = true;
  // Set a search direction to given value
  this->setSearchDirection(direction);
  // Repeat until a true search stop detected
  while(!stop_search) {
    this->Cntrl.SM = true; // Set search mode

    /* Step a grid step aside into given search direction */
    if(direction == tea5767_sud::Up)
      this->set_frequency(freqHz, +this->gridStep);
    else
      this->set_frequency(freqHz, -this->gridStep);
    this->sendRegisters();

    // Wait until ready flag is set, then check if end of the band reached
    do { this->recvRegisters(1); } while(!this->State.RF);

    // If accidentally end of the band occurred
    end_of_band = this->State.BLR;
    if(end_of_band)
    {
      if(direction == tea5767_sud::Up)
        freqHz = this->band_start;
      else
        freqHz = this->band_stop;
    }
    else
    {
      // Wait for level is ready
      usleep(TEA5767_LEV_TIMEOUT);
      this->recvRegisters();
      this->load_frequency();
      // Clear a search mode flag before a check
      this->Cntrl.SM = false;
      // Clear search mode before a checking the stop freq
      if(this->is_real_stop())
        stop_search = true; // It is a real stop, a search mode is already off
      else
      {
        // It is a fake stop, continue to search
        stop_search = false;
        freqHz = this->get_frequency();
      };
    };
  };

  this->Cntrl.SM = false;
  this->Cntrl.MUTE = mute_status;
  this->sendRegisters();

  // Return a flag that a radio band is started again
  return end_of_band;
};

/* Chek if it is a valid stop search or not */
bool
TEA5767::is_real_stop(void)
const
{
  uint8_t level1, level2, ifc;

  // Get available frequency with last side injection and frequency level
  long int freqHz = this->get_frequency(static_cast<tea5767_hlsi>(this->Cntrl.HLSI));
  level1 = this->getSignalLevel();

  // Set an opposite side injection for measure signal parameters
  this->Cntrl.HLSI = !this->Cntrl.HLSI;
  this->set_frequency(freqHz, static_cast<tea5767_hlsi>(this->Cntrl.HLSI)); // ...and frequency back

  // Start the measuring of IF count
  this->sendRegisters(); // Must be sent before measuring IFC
  usleep(TEA5767_IFC_TIMEOUT); // Wait for measuring IFC
  this->recvRegisters(); // Stop the measuring and get results
  // Get a measured IF count and level
  ifc = this->State.IF; // Stable until next write/27ms/read
  level2 = this->getSignalLevel();

  // Revert side injection and set wanted frequency back before exit
  this->Cntrl.HLSI = !this->Cntrl.HLSI;
  this->set_frequency(freqHz, static_cast<tea5767_hlsi>(this->Cntrl.HLSI)); // ...and frequency back
  this->sendRegisters();

  // Calculate if it is a real search stop
  bool real_stop = abs(level1 - level2) < 2
                && ifc > TEA5767_IFC_MIN && ifc < TEA5767_IFC_MAX;

  return real_stop;
};

/* Set a search stop level */
void
TEA5767::setSearchStopLevel(SSL search_stop_level)
const
{
  this->Cntrl.SSL = static_cast<uint8_t>(search_stop_level);
};

/* Set a search direction */
void
TEA5767::setSearchDirection(tea5767_sud direction)
const
{
  this->Cntrl.SUD = static_cast<bool>(direction);
};

/* Set a band limit */
void
TEA5767::setBand(tea5767_band band)
const
{
  this->Cntrl.BL = static_cast<bool>(band);
  switch(band)
  {
    case tea5767_band::Europe:
        this->band_start = 87500000;
        this->band_stop = 108000000;
      break;
    case tea5767_band::Japan:
        this->band_start = 76000000;
        this->band_stop = 91000000;
      break;
  };
};

/* Set a local oscilator to high (a high-side injection) or low */
void
TEA5767::setSideInjection(tea5767_hlsi hlsi)
const
{
  this->Cntrl.HLSI = static_cast<bool>(hlsi);
};

/* Enable or disable software programmable ports 1 and 2 */
void
TEA5767::setSWPort1(bool flag)
const
{
  this->Cntrl.SWP1 = flag;
};

void
TEA5767::setSWPort2(bool flag)
const
{
  this->Cntrl.SWP2 = flag;
};

/* Set or unset a mute of an audio output */
void
TEA5767::setMute(bool flag)
const
{
  this->Cntrl.MUTE = flag;
};

/* Force mono */
void
TEA5767::setMono(bool flag)
const
{
  this->Cntrl.MS = flag;
};

/* Set or unset a low powered standby mode */
void
TEA5767::setStandby(bool standby)
const
{
  this->Cntrl.STBY = standby;
};
