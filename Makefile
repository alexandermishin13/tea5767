CFLAGS=-c -Wall -O2
LDFLAGS=-ltea5767 -lc
INCLUDE=-I/usr/local/include
LIB=-L/usr/local/lib
PREFIX=/usr/local
MAJOR=1

all: libtea5767.so tea5767ctl

tea5767ctl: tea5767ctl.o
	$(CXX) $(LIB) $(LDFLAGS) -o $@ tea5767ctl.o

tea5767ctl.o: tea5767ctl.cpp
	$(CXX) $(CFLAGS) $(INCLUDE) $<

tea5767.o: tea5767.cpp tea5767.h
	$(CXX) -fpic $(CFLAGS) -o $@ tea5767.cpp

libtea5767.so: tea5767.o
	$(CXX) --shared -fpic -Wl,-soname,libtea5767.so -o libtea5767.so.${MAJOR} tea5767.o

clean:
	rm *.o *.so* demo tea5767ctl

demo.o: demo.cpp
	$(CXX) $(CFLAGS) $(INCLUDE) $<

demo: demo.o
	$(CXX) demo.o $(LIB) $(LDFLAGS) -o $@

install: libtea5767.so
	sudo install -m 0644 -o root -g wheel tea5767.h ${PREFIX}/include/
	sudo install -m 0755 -o root -g wheel -s libtea5767.* ${PREFIX}/lib/
	sudo install -lr ${PREFIX}/lib/libtea5767.so.${MAJOR} ${PREFIX}/lib/libtea5767.so

uninstall:
	sudo rm ${PREFIX}/include/tea5767.h
	sudo rm ${PREFIX}/lib/libtea5767.so*

check:
	cppcheck --enable=all --suppress=missingIncludeSystem .
