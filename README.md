# tea5767

FreeBSD library for control a tea5767 circuit over i2c or gpio

## Installation

For build and install library just do:

```
make
make install
```

You will be prompted to enter a password to elevate privileges,
and the library installation will be completed.

You also can build a ```demo``` by typing:

```
make demo

```

## Main public functions

Examples of main public functions:

* **TEA5767 radio(_32768_Hz);**
* **radio.setFrequency(freqMHz);**
* **radio.nextStation(TEA5767::SUD::Up);**

More functions You can learn from ```tea5767.h```

## Status

Current status of the library is beta and still developed.
