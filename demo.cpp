#include <stdio.h>
#include <unistd.h>
#include <tea5767.h>

#include <iostream>
#include <limits>

TEA5767 radio(tea5767_clock::_32768_Hz);

void wait()
{
    std::cout << "Press ENTER to continue...";
    std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
};

void station_info()
{
  printf("Freq: %f, mode: %s, hlsi: %s\n",
    radio.getFrequency(),
    radio.State.STEREO?"Stereo":"Mono",
    radio.Cntrl.HLSI?"High":"Low");
};

int
main(int argc, char **argv)
{
  radio.init(); // Default: TEA5767::Band::Europe
  radio.setSideInjection(tea5767_hlsi::High);
  radio.setSearchStopLevel(TEA5767::SSL::High); // Search stop level is middle(2)
  //radio.setFrequency(87.5);
  //radio.sendRegisters();
  //usleep(TEA5767_HLSI_TIMEOUT);
  radio.recvRegisters();
  station_info();
  while(true)
  {
    wait();
    radio.setMute(true);
    radio.nextStation(tea5767_sud::Up); // Search direction is UP
    usleep(TEA5767_IFC_TIMEOUT);
    radio.recvRegisters();
    radio.setFrequency(radio.getFrequency());
    radio.setMute(false);
    radio.sendRegisters(1);
    usleep(TEA5767_IFC_TIMEOUT);
    station_info();
  };
};
