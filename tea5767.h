//  Author:Alexander Mishin
//  Date:06 Nov, 2019
//
/*******************************************************************************/

#ifndef TEA5767_h
#define TEA5767_h

#include <sys/types.h>

/* I2C method */
#include <fcntl.h>
#include <dev/iicbus/iic.h>

#define TEA5767_DEVNAME "/dev/iic0"

#define TEA5767_ADDR 0x60

#define TEA5767_NUMBER_REGS 5
#define TEA5767_HLSI_IF 225000
#define TEA5767_LEV_TIMEOUT 9000
#define TEA5767_IFC_TIMEOUT 27000
#define TEA5767_IFC_MIN 0x31
#define TEA5767_IFC_MAX 0x3E
#define TEA5767_SEARCH_REPEATS 2

struct tea5767_write_regs
{
  /* register 1 */
  uint8_t hiPLL:6; // High part of PLL
  bool SM:1; // In search mode
  bool MUTE:1; // Muted left and right
  /* register 2 */
  uint8_t loPLL;  // Low part of PLL
  /* register 3 */
  bool SWP1:1; // Software programmable port 1
  bool ML:1; // Muted left and forced mono
  bool MR:1; // Muted right and forced mono
  bool MS:1; // Forced mono to stereo
  bool HLSI:1; // High/Low Side injection
  uint8_t SSL:2; // Search Stop Level 01=Low, 10=middle, 11=high
  bool SUD:1; // Search direction is up (or down)
  /* register 4 */
  bool SI:1; //if SWPORT1 is output for the ready or just programmable port
  bool SNC:1; //Stereo Noise Cancelling
  bool HCC:1; // High Cut Control
  bool SMUTE:1; // Soft Mute
  bool XTAL:1; // Crystal clock frequency (PLLREF=0 and XTAL=1 freq=32.768KHz)
  bool BL:1; // is the Band limits Japanese (or US/EU)
  bool STBY:1; // Standby mode
  bool SWP2:1; // Software programmable port 2
  /* register 5 */
  uint8_t RESERVED5:6;
  bool DTC:1; // Is the de-emphases time constant 75us (or 50us)
  bool PLLREF:1; // See XTAL
};

struct tea5767_read_regs
{
  /* register 1 */
  uint8_t hiPLL:6; // High part of PLL
  bool BLR:1;     // Band limit reached
  bool RF:1;      // Ready flag
  /* register 2 */
  uint8_t loPLL;    // Low part of PLL
  /* register 3 */
  uint8_t IF:7;   // IF counter
  bool STEREO:1;
  /* register 4 */
  uint8_t RESERVED4:1;  // Not used
  uint8_t CI:3;      // Chip ID
  uint8_t LEV:4;   // Radio level
  /* register 5 */
  uint8_t RESERVED5;      // Not used
};

enum class tea5767_clock: uint8_t {
  _32768_Hz = 0,
  _13_MHz = 1,
  _13000_KHz = 1,
  _6500_KHz = 2
};

enum class tea5767_band: bool {
  Japan = true,
  Europe = false,
  US = false
};

enum class tea5767_hlsi: bool {
  High = true,
  Low = false
};

enum class tea5767_sud: bool {
      Up = true,
      Down = false
};

static const char defaultDevName[] = TEA5767_DEVNAME;

class TEA5767
{
  public:
    explicit TEA5767(
      enum tea5767_clock clock,
      const char* iicDevice = defaultDevName,
      uint8_t slaveAddress = TEA5767_ADDR
    );
    ~TEA5767(void);

    enum class SSL: uint8_t {
      Low = 0b00000001,
      Middle = 0b00000010,
      High = 0b00000011
    };

    /* Registers arrays and structures */
    union
    {
      mutable uint8_t writeRegisters[TEA5767_NUMBER_REGS] = {0};
      mutable struct tea5767_write_regs Cntrl;
    };
    union {
      mutable uint8_t readRegisters[TEA5767_NUMBER_REGS] = {0};
      struct tea5767_read_regs State;
    };

    void init(tea5767_band band=tea5767_band::Europe) const;
    void printState(void) const;
    /* Set a wanted frequency (taking in account a HLSI flag)
       bool tune - do HLSI optimization */
    void setFrequency(double freqMHz, bool tune = true) const;
    /* Get a wanted frequency (taking in account a HLSI flag) */
    double getFrequency(void) const;
    /* Get a RF signal level */
    uint8_t getSignalLevel(void) const;
    /* Set a search stop level {LOW, MIDDLE, HIGH} */
    void setSearchStopLevel(SSL) const;
    /* Set search direction { UP, DOWN } */
    void setSearchDirection(tea5767_sud direction) const;
    /* Set a band limit { Japan, Europe (or US)} */
    void setBand(tea5767_band) const;
    /* Set a high/low side injection */
    void setSideInjection(tea5767_hlsi) const;
    /* Search next station in a given direction */
    bool nextStation(tea5767_sud direction) const;
    /* Mute or unmute an audio output */
    void setMute(bool) const;
    /* Force mono */
    void setMono(bool) const;
    /* Enable or disable either of two software programmable ports */
    void setSWPort1(bool swport_enable) const;
    void setSWPort2(bool swport_enable) const;
    /* Set a low powered standby mode */
    void setStandby(bool standby) const;
    /* Send from writeRegisters[] or receive to readRegisters[]
       device registers (less than five is allowed by its datasheet) */
    bool sendRegisters(uint8_t size = TEA5767_NUMBER_REGS) const;
    bool recvRegisters(uint8_t size = TEA5767_NUMBER_REGS) const;

  private:
    /* Not implemented but deny public operations */
    TEA5767(const TEA5767 &that);
    TEA5767& operator=(const TEA5767 &that);

  protected:
    int iic_handle;
    uint8_t slave_addr;
    bool xtal = true;
    bool pllRef = true;
    unsigned int refFreqHz = 0;
    int gridStep = 0;
    mutable long int band_start;
    mutable long int band_stop;
    /* I2C functions */
    bool iic_write(uint8_t buffer[], uint16_t size = TEA5767_NUMBER_REGS) const;
    bool iic_read(uint8_t buffer[], uint16_t size = TEA5767_NUMBER_REGS) const;

    /* Detect side injection mode */
    tea5767_hlsi side_injection(long int freqHz) const;
    /* Get frequency (Hz) with taking in account an intermediate frequency (Hz) */
    long int get_frequency(long int imFreqHz = 0) const;
    long int get_frequency(tea5767_hlsi hlsi) const;
    /* Set a frequency with taking in account an intermediate frequency */
    void set_frequency(long int freqHz, long int imFreqHz = 0) const;
    void set_frequency(long int freqHz, tea5767_hlsi hlsi) const;
    /* Load an actual frequency from device and fill to registers for write */
    void load_frequency(void) const;
    /* Check a stop search validness */
    bool is_real_stop(void) const;
};

#endif
