//  Author:Alexander Mishin
//  Date:17 Dec, 2019
//
/*******************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <err.h>
#include <tea5767.h>

void
usage(void)
{
  printf(
    "--band <\"Europe\"|\"US\"|\"Japan\">: Set a radio band\n"
    "--freq <float freqMHz>: Set a wanted frequency\n"
    "--scan <\"up\"|\"down\">:   Scan the radio band up or down\n"
    "--verbose:              Be more verbose\n"
    "--help:                 Show this help\n"
  );
  exit(1);
};

int
main(int argc, char **argv)
{
  TEA5767 radio(tea5767_clock::_32768_Hz);
  double freqMHz;
  tea5767_band band = tea5767_band::Europe;
  tea5767_sud scan = tea5767_sud::Up;

  int set_freq_flag = 0;
  int set_scan_flag = 0;
  int verbose_flag = 0;

  const char* const opts_short = "b:s:f:hv";
  const option opts_long[] = {
    {"band", required_argument, NULL, 'b'},
    {"freq", required_argument, NULL, 'f'},
    {"scan", required_argument, NULL, 's'},
    {"help", no_argument, NULL, 'h'},
    {"verbose", no_argument, &verbose_flag, 1},
    {NULL, 0, NULL, 0}
  };

  while(true)
  {
    const auto opt = getopt_long(argc, argv, opts_short, opts_long, nullptr);

    if(opt == -1)
    {
      if(argc <=1)
        usage();
      break;
    };

    switch(opt)
    {
      case 0:
        break;
      case 'f':
          freqMHz = std::stod(optarg);
          set_freq_flag = 1;
        break;
      case 's':
          if(strcasecmp(optarg, "up") == 0)
            scan = tea5767_sud::Up;
          else if(strcasecmp(optarg, "down") == 0)
            scan = tea5767_sud::Down;
          set_scan_flag = 1;
        break;
      case 'b': // Get a radio band if one is known
          if(strcasecmp(optarg, "japan") == 0)
            band = tea5767_band::Japan;
          else if(strcasecmp(optarg, "europe") == 0)
            band = tea5767_band::Europe;
          else if(strcasecmp(optarg, "us") == 0)
            band = tea5767_band::US;
          else
            err(1, "Wrong radio-band \"%s\"\n", optarg);
        break;
      case 'h':
      case '?':
      default:
          usage();
        break;
    };
  };

  // Init the radio
  if(verbose_flag)
    printf("Radio-band: %s\n", optarg);

  radio.init(band);

  // Set wanted frequency if was set one
  if(set_freq_flag)
  {
    if(verbose_flag)
      printf("Frequency: %fMHz\n", freqMHz);

    radio.setFrequency(freqMHz);
    freqMHz = radio.getFrequency();
  };

  // Scan the radio band for next station
  if(set_scan_flag)
  {
    if(verbose_flag)
      printf("Scan %s\n", scan == tea5767_sud::Up ? "up" : "down");
    radio.nextStation(scan);
    usleep(TEA5767_IFC_TIMEOUT);
    freqMHz = radio.getFrequency();
    radio.setFrequency(freqMHz);
    if(verbose_flag)
      printf("New frequency: %fMHz\n", freqMHz);
  };
};
